import { render, screen } from '@testing-library/react';
import NotFound from './404';

describe('NotFound component', () => {
  it('should render the 404 message', () => {
    render(<NotFound />);
    const linkElement = screen.getByText(/404/i);
    expect(linkElement).toBeInTheDocument();
  });
});
