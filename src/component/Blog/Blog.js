import React, { Component } from 'react';
import Sectiontitle from '../Banner/Sectiontitle';
import BlogItems from '../Blog/BlogItems';
import { Fade } from 'react-awesome-reveal';
class Blog extends Component {
  render() {
    return (
      <section className='blog_area' id='blog'>
        <div className='container'>
          <Sectiontitle
            Title='Aktuelles'
            TitleP='It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'
          />
          <Fade direction='up' cascade triggerOnce duration={1000}>
            <div className='row'>
              <BlogItems bTitle='Headless CMS' bDetails='Lorem ipsum' image='post-img1.jpg' />
              <BlogItems
                bTitle='App Insights'
                bDetails='Lorem ipsum'
                // bDetails="Data Science mit Python"
                image='post-img2.jpg'
              />
              <BlogItems bTitle='Cyber Security' bDetails='Lorem ipsum' image='post-img3.jpg' />
            </div>
          </Fade>
        </div>
      </section>
    );
  }
}
export default Blog;
