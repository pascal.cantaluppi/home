import React, { Component } from 'react';
import SectionTitleTwo from '../Banner/SectionTitleTwo';
import BlogTwoitems from '../Blog/BlogTwoitems';
import { Fade } from 'react-awesome-reveal';
class BlogTwo extends Component {
  render() {
    return (
      <section className='blog_area_two' id='blog'>
        <div className='container'>
          <SectionTitleTwo tCenter='text-center' stitle='Aktuelles' btitle='News and Updates' />
          <Fade direction='fadeInUp' duration={1000}>
            <div className='row'>
              <BlogTwoitems
                bTitle='Headless CMS'
                bDetails='Suspendisse in mattis neque, sed accu- msan erat. Maecenas eget metus dui. Vestibulum accumsan massa quam..'
                btnText='Read More'
                image='post-img1.jpg'
                Pdata='Jan 14'
              />
              <BlogTwoitems
                bTitle='Behind the color'
                bDetails='Suspendisse in mattis neque, sed accu- msan erat. Maecenas eget metus dui. Vestibulum accumsan massa quam..'
                btnText='Read More'
                image='post-img2.jpg'
                Pdata='Mar 30'
              />
            </div>
          </Fade>
        </div>
      </section>
    );
  }
}
export default BlogTwo;
