import React, { Component } from 'react';
import { Fade } from 'react-awesome-reveal';
class FooterTwo extends Component {
  render() {
    let cmsData = this.props.cmsData;
    let { className } = this.props;
    return (
      <section className={`footer-area-two ${className}`}>
        <div className='container'>
          <div className='row footer-content'>
            <div className='col-sm-4'>
              <Fade direction='up' cascade triggerOnce>
                <ul className='list_style'>
                  {cmsData.socialLinks.map((item) => {
                    return (
                      <li key={item.name}>
                        <a href={item.url} target='_blank' rel='noreferrer'>
                          <i className={item.className}></i>
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </Fade>
            </div>
            <div className='col-sm-8 text-right'>
              <h6>
                Made with love by <a href='https://themeforest.net/user/blazethemez'>Blazethemez</a>
              </h6>
              <p>© 2020 Blazethemez - All Rights Reserved</p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default FooterTwo;
