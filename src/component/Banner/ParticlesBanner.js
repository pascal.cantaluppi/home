import React, { Component } from 'react';
import Particles from './Particles';
import { Fade } from 'react-awesome-reveal';
import Typer from './Typer';

class ParticlesBanner extends Component {
  render() {
    let cmsData = this.props.cmsData;
    return (
      <section id='home'>
        <div className='banner_area_two'>
          <div className='one_img' data-parallax='{"x": 0, "y": 100, "rotateZ":0}'>
            <img className='animated' src={require('../../image/circle-2.png')} alt='' />
          </div>
          <Particles />
          <div className='container'>
            <div className='banner_content'>
              <Fade direction='up'>
                <h2 className='wow fadeInLeft animated'>I'm {cmsData.name2}</h2>
              </Fade>
              <Fade direction='up' duration={1500}>
                <h3>{cmsData.ptext}</h3>
              </Fade>
              <Typer
                heading={'Specialized in'}
                dataText={['Developer.', 'Photographer.', 'Designing UI/UX.', 'Freelancer.']}
              />
              <ul className='list_style social_icon'>
                {cmsData.socialLinks &&
                  cmsData.socialLinks.map((item) => {
                    return (
                      <li key={item.name}>
                        <a href={item.url} target='_blank' rel='noreferrer'>
                          <i className={item.className}></i>
                        </a>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default ParticlesBanner;
