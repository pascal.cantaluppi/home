import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import NotFound from './component/404';
import Home from './Home';

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='*' element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
